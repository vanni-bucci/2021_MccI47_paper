# 2021 MccI47 Paper

## Getting started
This projects contains the code for the statistical analysis of the manuscript:

Microcin MccI47 potently inhibits multidrug-resistant enteric bacteria and reduces Klebsiella pneumoniae colonization in vivo when administered via an engineered live biotherapeutic

Authors: Benedikt M. Mortzfeld1,2, Jacob D. Palmer3,4, Shakti K. Bhattarai1,2, Haley L. Dupre5, Regino Mercado-Lubo1, Mark W. Silby6, Corinna Bang7, Beth A. McCormick1,2, Vanni Bucci1,2,8 

Affiliations:
1Department of Microbiology and Physiological Systems, University of Massachusetts Medical School, Worcester, MA, USA
2Program in Microbiome Dynamics, University of Massachusetts Medical School, Worcester, MA, USA
3Department of Zoology, University of Oxford, Oxford, United Kingdom
4Department of Biochemistry, University of Oxford, Oxford, United Kingdom
5Department of Bioengineering, University of Massachusetts Dartmouth, North Dartmouth, MA, USA
6Department of Biology, University of Massachusetts Dartmouth, Dartmouth MA, USA
7Institute of Clinical Molecular Biology, Christian-Albrechts-Universität zu Kiel, Kiel, Germany
8Program in Systems Biology, University of Massachusetts Medical School, Worcester, MA, USA

Correspondence to:
Vanni Bucci  vanni.bucci2@umassmed.edu
Benedikt Mortzfeld benedikt.mortzfeld@umassmed.edu

## Files available
Code and data deposited allows to reproduce the figures of the manuscript.

## Important Note
This project is updated as necessary while we go trough the appropriate peer-review process of the manuscript.



